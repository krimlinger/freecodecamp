class DisplayMessages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      messages: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitMessage = this.submitMessage.bind(this);
  }
  // add handleChange() and submitMessage() methods here
  handleChange(ev) {
    this.setState({input: ev.target.value});
  }
  submitMessage(ev) {
    this.setState(({input, messages}) => ({
      input: '',
      messages: messages.concat([input]),
    }));
  }
  render() {
    return (
      <div>
        <h2>Type in a new Message:</h2>
        { /* render an input, button, and ul here */ }
        <input
          value={this.state.input}
          onChange={this.handleChange}
        />
        <button onClick={this.submitMessage}>
          Submit message
        </button>
        <ul>
          {this.state.messages.map(x => (
            <li key={x+1}>{x}</li>
          ))}
        </ul>
        { /* change code above this line */ }
      </div>
    );
  }
};
