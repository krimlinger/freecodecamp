function rangeOfNumbers(startNum, endNum) {
  if (startNum > endNum) {
    return [];
  }
  const a = rangeOfNumbers(startNum + 1, endNum);
  a.unshift(startNum);
  return a;
}

