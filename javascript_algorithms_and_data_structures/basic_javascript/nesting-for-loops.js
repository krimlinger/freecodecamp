function multiplyAll(arr) {
  var product = 1;
  // Only change code below this line
  for (let subArr of arr) {
    for (let e of subArr) {
      product *= e;
    }
  }
  // Only change code above this line
  return product;
}

// Modify values below to test your code
multiplyAll([[1,2],[3,4],[5,6,7]]);
