function checkSign(num) {
    return !num ? "zero": (num < 0 ? "negative": "positive");
}

checkSign(10);
