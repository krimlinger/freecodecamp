const squareList = (arr) => {
  // only change code below this line
  return arr.filter(x => Number.isInteger(x) && x > 0)
            .map(x => x**2);
  // only change code above this line
};

// test your code
const squaredIntegers = squareList([-3, 4.8, 5, 3, -3.2]);
console.log(squaredIntegers);
