const STR_TO_AMOUNT = new Map([
  ["ONE HUNDRED", 100],
  ["TWENTY", 20],
  ["TEN", 10],
  ["FIVE", 5],
  ["ONE", 1],
  ["QUARTER", 0.25],
  ["DIME", 0.1],
  ["NICKEL", 0.05],
  ["PENNY", 0.01],
]);

function compareAmountArrayDec(a, b) {
  const sortedAmounts = [...STR_TO_AMOUNT.keys()]
  return sortedAmounts.indexOf(a[0]) - sortedAmounts.indexOf(b[0]);
}

function isCidEqualChange(cid, change) {
    const nonulCid = cid.filter(x => x[1] != 0);
    return nonulCid.length == change.change.length &&
           nonulCid.every((elem, i) => elem[1] == change.change[i][1]);
}

function checkCashRegister(price, cash, cid) {
  let change = { change: [] };
  let changeNum = cash - price;
  if (changeNum < 0) {
    return {status: "INSUFFICIENT_FUNDS", change: []};
  }
  cid.sort(compareAmountArrayDec);
  for (let [kind, value] of cid) {
    const unitValue = STR_TO_AMOUNT.get(kind);
    let changeValue = 0;
    while (value > 0 && changeNum - unitValue >= 0) {
      value -= unitValue;
      changeNum -= unitValue;
      changeValue += unitValue;
      // Fix rounding error like a monkey.
      changeValue = Math.round(changeValue*100)/100;
      value = Math.round(value*100)/100;
      changeNum = Math.round(changeNum*100)/100;
    }
    if (changeValue > 0) {
      change.change.push([kind, changeValue]);
    }
  }
  change.change.sort(compareAmountArrayDec);
  if (changeNum != 0) {
    change.status = "INSUFFICIENT_FUNDS";
    change.change = [];
  } else {
    //const nonulCid = cid.filter(x => x[1] != 0);
    //(nonulCid.length == change.change.length &&
    // nonulCid.every((elem, i) => elem[1] == change.change[i][1])) {
    if (isCidEqualChange(cid, change)) {
      let c = [...cid].sort((x, y) => compareAmountArrayDec(y, x));
      change.status = "CLOSED";
      change.change = c;
    } else {
      change.status = "OPEN";
    }
  }
  return change;
}

// Example cash-in-drawer array:
// [["PENNY", 1.01],
// ["NICKEL", 2.05],
// ["DIME", 3.1],
// ["QUARTER", 4.25],
// ["ONE", 90],
// ["FIVE", 55],
// ["TEN", 20],
// ["TWENTY", 60],
// ["ONE HUNDRED", 100]]

checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);
