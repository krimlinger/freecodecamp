
const ROMAN_TABLE = new Map([
    ["M", 1000],
    ["CM", 900],
    ["D", 500],
    ["CD", 400],
    ["C", 100],
    ["XC", 90],
    ["L", 50],
    ["XL", 40],
    ["X", 10],
    ["IX", 9],
    ["V", 5],
    ["IV", 4],
    ["I", 1],
]);

function convertToRoman(num) {
    let ret = "";
    while (num) {
        for (const [roman, dec] of ROMAN_TABLE) {
            if (num - dec >= 0) {
                num -= dec;
                ret += roman;
                break;
            }
        }
    }
    return ret;
}

convertToRoman(36);
