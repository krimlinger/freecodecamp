function rot13(str) { // LBH QVQ VG!
  let ret = "";
  for (let c of str) {
    if (c < "A" || c > "Z") {
      ret += c;
      continue;
    }
    ret += String.fromCharCode("A".charCodeAt() + (c.charCodeAt() - "A".charCodeAt() + 13) % ("Z".charCodeAt() - "A".charCodeAt()+1));
  }
  return ret;
}

// Change the inputs below to test
console.log(rot13("SERR PBQR PNZC"));
