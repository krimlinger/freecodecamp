function palindrome(str) {
  const clearStr = str.replace(/\W|_/g, "").toLowerCase();
  for (let i = 0; i < Math.floor(clearStr.length/2); i++) {
    if (clearStr[i] != clearStr[clearStr.length - i - 1]) {
      return false;
    }
  }
  return true;
}

palindrome("eye");
