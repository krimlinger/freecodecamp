let users = {
  Alan: {
    age: 27,
    online: true
  },
  Jeff: {
    age: 32,
    online: true
  },
  Sarah: {
    age: 48,
    online: true
  },
  Ryan: {
    age: 19,
    online: true
  }
};
let keys = Array.from(Object.keys(users));

function isEveryoneHere(obj) {
  // change code below this line
  return keys.every(x => x in obj);
   // change code above this line
}

console.log(isEveryoneHere(users));
 