function sumAll(arr) {
  let max = Math.max(...arr);
  let min = Math.min(...arr) - 1;
  return (max*(max+1)/2) - (min*(min+1)/2);
}

sumAll([1, 4]);
