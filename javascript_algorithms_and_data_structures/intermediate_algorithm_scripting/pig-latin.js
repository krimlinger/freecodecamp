function translatePigLatin(str) {
  let consonantStart = 0;
  for (const c of str) {
    if ("aeiou".includes(c)) {
      break;
    }
    consonantStart++;
  }
  if (consonantStart) {
    return str.slice(consonantStart) + str.slice(0, consonantStart) + "ay";
  } else {
    return str + "way"; 
  }
}

translatePigLatin("consonant");
