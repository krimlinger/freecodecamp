function dropElements(arr, func) {
  let i = 0;
  for (; i < arr.length; i++) {
    if (func(arr[i])) {
      break;
    }
  }
  return arr.slice(i);
}

dropElements([1, 2, 3], function(n) {return n < 3; });
