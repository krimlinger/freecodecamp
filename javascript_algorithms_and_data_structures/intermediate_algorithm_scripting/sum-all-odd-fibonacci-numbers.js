function sumFibs(num) {
  let firstPrev = 1;
  let secPrev = 1;
  let sum = 2;
  if (num <= 1) {
    return 1;
  }
  for (let i = 1; i < num; i++) {
    const newPrev = firstPrev + secPrev;
    secPrev = firstPrev;
    firstPrev = newPrev;
    if (newPrev > num) {
      return sum;
    }
    if (newPrev % 2 != 0) {
      sum += newPrev;
    }
  }
  return sum;
}

sumFibs(4);
