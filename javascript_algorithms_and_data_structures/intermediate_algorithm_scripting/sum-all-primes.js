function sumPrimes(num) {
  let sum = 0;
  for (let i = 2; i <= num; i++) {
    if (isPrime(i)) {
      sum += i;
    }
  }
  return sum;
}

function isPrime(n) {
  for (let j = n; j > 1; j--) {
    if (Number.isInteger(n/j) && j != n) {
      return false;
    }
  }
  return true;
}

console.log(sumPrimes(10));
