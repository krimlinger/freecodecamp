function convertHTML(str) {
  let ret = "";
  for (const char of str) {
    switch (char) {
      case "&":
        ret += "&amp;";
        break;
      case "<":
        ret += "&lt;";
        break;
      case ">":
        ret += "&gt;";
        break;
      case "'":
        ret += "&apos;";
        break;
      case '"':
        ret += "&quot;";
        break;
      default:
        ret += char;
        break;
    }
  }
  return ret;
}

convertHTML("Dolce & Gabbana");
