var Person = function(firstAndLast) {
  let [_firstName, _lastName] = firstAndLast.split(" ");
  this.getFullName = function() {
    return `${_firstName} ${_lastName}`;
  };
  this.getFirstName = function() {
    return _firstName;
  }
  this.getLastName = function() {
    return _lastName;
  }
  this.setFirstName = function(val) {
    _firstName = val;
  }
  this.setLastName = function(val) {
    _lastName = val;
  }
  this.setFullName = function(val) {
    [_firstName, _lastName] = val.split(" ");
  }
  return firstAndLast;
};

var bob = new Person('Bob Ross');
bob.getFullName();
