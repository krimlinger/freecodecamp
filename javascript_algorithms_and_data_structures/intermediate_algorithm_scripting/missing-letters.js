function fearNotLetter(str) {
  if (str.length == 0) {
    return;
  }
  let i = 1;
  for (const c of str.substring(1)) {
    if (c.charCodeAt() - str.charCodeAt(i-1) != 1) {
      return String.fromCharCode(c.charCodeAt() - 1);
    }
    i++;
  }
}

fearNotLetter("abce");
