function spinalCase(str) {
  let ret = "";
  ret = str.replace(/([A-z])([a-z]*)/g, "$1$2 ")
           .replace(/_|-/g, " ")
           .trim()
           .replace(/\s+/g, "-")
           .toLowerCase();
  return ret;
}

spinalCase('This Is Spinal Tap');
console.log(spinalCase('The_Andy_Griffith_Show'));
