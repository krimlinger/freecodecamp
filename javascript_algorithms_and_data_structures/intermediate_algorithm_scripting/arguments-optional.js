function addTogether(...args) {
  if (typeof args[0] !== "number" || args.length < 1) {
    return;
  }
  if (args.length > 1) {
    if (typeof args[1] !== "number") {
      return;
    }
    return args[0] + args[1];
  }

  return (function (x) {
    if (typeof x !== "number") {
      return;
    }
    return args[0] + x;
  });
}

addTogether(2,3);
