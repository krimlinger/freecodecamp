function destroyer(arr, ...args) {
  const targets = new Set(args);
  let ret = [];
  for (const [i, elem] of arr.entries()) {
    if (!targets.has(elem)) {
      ret.push(elem);
    }
  }
  return ret;
}

destroyer([1, 2, 3, 1, 2, 3], 2, 3);
