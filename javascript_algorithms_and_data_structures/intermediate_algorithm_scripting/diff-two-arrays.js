function diffArray(arr1, arr2) {
  return arr1.filter(e => !arr2.includes(e))
             .concat(arr2.filter(e => !arr1.includes(e)));
}

diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]);
