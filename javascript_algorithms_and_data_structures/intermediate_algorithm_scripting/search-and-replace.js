function myReplace(str, before, after) {
  const regexp = new RegExp(before);
  const regexpMatch = str.match(regexp)[0];
  let myAfter = after;
  if (regexpMatch[0] == regexpMatch[0].toUpperCase()) {
    myAfter = after[0].toUpperCase() + after.substring(1);
  }
  return str.replace(regexp, myAfter);
}

myReplace("A quick brown fox jumped over the lazy dog", "jumped", "leaped");
console.log(myReplace("He is Sleeping on the couch", "Sleeping", "sitting"));