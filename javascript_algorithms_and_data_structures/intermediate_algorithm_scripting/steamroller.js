function steamrollArray(arr) {
  console.log(`treat ${arr}`);
  let ret = [];
  for (const e of arr) {
    if (Array.isArray(e)) {
      ret = ret.concat(steamrollArray(e));
    } else {
      ret.push(e);
    }
  }
  return ret;
}

steamrollArray([1, [2], [3, [[4]]]]);
