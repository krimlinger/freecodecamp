function uniteUnique(...arr) {
  const set = new Set([].concat(...arr));
  return Array.from(set);
}

uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1]);
