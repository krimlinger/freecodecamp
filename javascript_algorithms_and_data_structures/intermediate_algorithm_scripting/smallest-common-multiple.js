function smallestCommons(arr) {
  arr.sort((a, b) => a-b);
  let [min, max] = arr;
  let ret = max;
  for (let i = max - 1; i >= min; i--) {
    if (ret % i != 0) {
      i = max;
      ret += max;
    }
  }
  return ret;
}


smallestCommons([1,5]);
