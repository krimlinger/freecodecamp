function mutation(arr) {
  let charMap = new Map();
  for (let e of arr[0]) {
    charMap.set(e.toLowerCase(), true);
  }
  return Array.from(arr[1]).every(x => charMap.has(x.toLowerCase()));
}

mutation(["hello", "hey"]);
