function findElement(arr, func) {
  for (let e of arr) {
    if (func(e)) {
      return e;
    }
  }
  return undefined;
}

findElement([1, 2, 3, 4], num => num % 2 === 0);
