function findLongestWordLength(str) {
  return str.split(" ").reduce((acc, val) => val.length > acc.length ? val: acc, "").length;
}

console.log(findLongestWordLength("The quick brown fox jumped over the lazy dog"));
