function repeatStringNumTimes(str, num) {
  let ret = "";
  for (let i = num; i > 0; i--) {
    ret += str;
  }
  return ret;
}

repeatStringNumTimes("abc", 3);
