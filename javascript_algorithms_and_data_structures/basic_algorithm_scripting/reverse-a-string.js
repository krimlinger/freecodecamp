function reverseString(str) {
  return Array.from(str).reduce((acc, val) => val + acc, "");
}

console.log(reverseString("hello"));
