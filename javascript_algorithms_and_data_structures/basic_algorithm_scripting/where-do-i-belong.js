function getIndexToIns(arr, num) {
  return arr.reduce((acc, val) => num > val ? acc+1: acc, 0);
}

getIndexToIns([40, 60], 50);
