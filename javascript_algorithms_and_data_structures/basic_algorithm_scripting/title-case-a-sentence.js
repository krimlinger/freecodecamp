function titleCase(str) {
  let ret = "";
  let prevSpace = true;
  for (let char of str) {
    if (prevSpace) {
      ret += char.toUpperCase();
      prevSpace = false;
    } else {
      ret += char.toLowerCase()
    }
    if (char == " ") {
      prevSpace = true;
      continue;
    }
  }
  return ret;
}

titleCase("I'm a little tea pot");
