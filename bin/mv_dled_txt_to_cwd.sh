#!/usr/bin/env sh

# Move all .txt files from download directory to cwd
# and change their file extension to .html.
# Mainly used to version fcc challenges.

DOWNLOAD_DIR="$(xdg-user-dir DOWNLOAD)"
EXT=".html"

if [ -z "$DOWNLOAD_DIR" ]; then
    echo "could not get XDG DOWNLOAD directory, exiting"
    exit 1
fi

if [ -n "$1" ]; then
    EXT="$1"
fi

for f in "$DOWNLOAD_DIR"/*.txt; do
    mv "$f" "$(basename $f .txt)$EXT"
done
